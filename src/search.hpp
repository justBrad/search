int search (std::string theText, std::string thePattern){
	int index=0, pdex=0;
	bool flag=false;
	while (index < (int)theText.size() && !flag){
		if (theText[index] == thePattern[pdex]){
			pdex++;
			index++;
			if (pdex >= (int)thePattern.size()){
				flag=true;
			}
		} else{
			index++;
			pdex=0;
		}
	}
	if (!flag)
		return -1;
	else
		return index-thePattern.size();
}